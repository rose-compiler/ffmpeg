static void mlp_filter_channel_x86(int32_t *state, const int32_t *coeff,
                                   int firorder, int iirorder,
                                   unsigned int filter_shift, int32_t mask,
                                   int blocksize, int32_t *sample_buffer)
{
    const void *firjump = firtable[firorder];
    const void *iirjump = iirtable[iirorder];

    blocksize = -blocksize;

    __asm__ volatile(
        "1:                           \n\t"
        CLEAR_ACCUM
        "jmp  *%5                     \n\t"
        FIRMUL   (ff_mlp_firorder_8, 0x1c   )
        FIRMUL   (ff_mlp_firorder_7, 0x18   )
        FIRMUL   (ff_mlp_firorder_6, 0x14   )
        FIRMUL   (ff_mlp_firorder_5, 0x10   )
        FIRMUL   (ff_mlp_firorder_4, 0x0c   )
        FIRMULREG(ff_mlp_firorder_3, 0x08,10)
        FIRMULREG(ff_mlp_firorder_2, 0x04, 9)
        FIRMULREG(ff_mlp_firorder_1, 0x00, 8)
        LABEL_MANGLE(ff_mlp_firorder_0)":\n\t"
        "jmp  *%6                     \n\t"
        IIRMUL   (ff_mlp_iirorder_4, 0x0c   )
        IIRMUL   (ff_mlp_iirorder_3, 0x08   )
        IIRMUL   (ff_mlp_iirorder_2, 0x04   )
        IIRMUL   (ff_mlp_iirorder_1, 0x00   )
        LABEL_MANGLE(ff_mlp_iirorder_0)":\n\t"
        SHIFT_ACCUM
        "mov  "RESULT"  ,"ACCUM"      \n\t"
        "add  (%2)      ,"RESULT"     \n\t"
        "and   %4       ,"RESULT"     \n\t"
        "sub   $4       ,  %0         \n\t"
        "mov  "RESULT32", (%0)        \n\t"
        "mov  "RESULT32", (%2)        \n\t"
        "add $"BINC"    ,  %2         \n\t"
        "sub  "ACCUM"   ,"RESULT"     \n\t"
        "mov  "RESULT32","IOFFS"(%0)  \n\t"
        "incl              %3         \n\t"
        "js 1b                        \n\t"
        : /* 0*/"+r"(state),
          /* 1*/"+r"(coeff),
          /* 2*/"+r"(sample_buffer),
#if ARCH_X86_64
          /* 3*/"+r"(blocksize)
        : /* 4*/"r"((x86_reg)mask), /* 5*/"r"(firjump),
          /* 6*/"r"(iirjump)      , /* 7*/"c"(filter_shift)
        , /* 8*/"r"((int64_t)coeff[0])
        , /* 9*/"r"((int64_t)coeff[1])
        , /*10*/"r"((int64_t)coeff[2])
        : "rax", "rdx", "rsi"
#else /* ARCH_X86_32 */
          /* 3*/"+m"(blocksize)
        : /* 4*/"m"(         mask), /* 5*/"m"(firjump),
          /* 6*/"m"(iirjump)      , /* 7*/"m"(filter_shift)
        : "eax", "edx", "esi", "ecx"
#endif /* !ARCH_X86_64 */
    );
}
